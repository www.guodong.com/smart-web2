package cn.com.smart.report.service;

import java.util.Map;

/**
 * 报表SQL资源回调接口
 */
public interface IReportSqlResourceCallback {

    /**
     * 回调方法
     * @param sql
     * @param param
     * @return
     */
    String callback(String sql, Map<String, Object> param);

}
